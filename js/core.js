document.addEventListener('DOMContentLoaded', function () {
    var cy = cytoscape({
        container: document.getElementById('cy'),
        elements: [],
        style: [
            {
                selector: 'node',
                style: {
                    'background-color': '#3cc564',
                    'label': 'data(label)',
                    'color': 'white',
                    'border-width': 2,
                    'border-color': '#fff',
                    'border-opacity': 0.7,
                    'text-outline-color': '#3cc564',
                    'text-outline-width': 1,
                }
            },
            {
                selector: 'node:selected',
                style: {
                    'background-color': '#c53c9d',
                    'text-outline-color': '#c53c9d',
                }
            },
            {
                selector: 'edge',
                style: {
                    'width': 2,
                    'line-color': '#ccc',
                    'curve-style': 'bezier',
                    'target-arrow-shape': 'triangle',
                    'target-arrow-color': '#ccc',
                    'source-arrow-shape': 'none'
                }
            },
            {
                selector: 'edge:selected',
                style: {
                    'line-color': '#c5a93c',
                    'target-arrow-color': '#c5a93c',
                    'width': 3
                }
            }
        ]
    });

    var actionHistory = [];
    var actionIndex = -1;

    // Funzione per aggiungere un'azione all'elenco delle azioni
    function addAction(actions) {
        if (actionIndex < actionHistory.length - 1) {
            actionHistory = actionHistory.slice(0, actionIndex + 1);
        }
        actionHistory.push(actions);
        actionIndex = actionHistory.length - 1;
    }

    // Funzione per visualizzare lo stato dei nodi e dei collegamenti
    function logCurrentState() {
        console.log('Nodi attuali:', cy.nodes().map(node => node.data()));
        console.log('Collegamenti attuali:', cy.edges().map(edge => edge.data()));
    }

    // Funzione per aprire il modale
    document.getElementById('openModalButton').addEventListener('click', function () {
        document.getElementById('modal-input').value = '';
        document.getElementById('modal').style.display = 'block';
    });

    // Funzione per aprire il modale per cambiare etichetta
    document.getElementById('openChangeLabelModalButton').addEventListener('click', function () {
        var selectedNodes = cy.$('node:selected');
        if (selectedNodes.length === 0) {
            alert('Seleziona almeno un nodo da modificare.');
        } else {
            document.getElementById('modal').style.display = 'block';
        }
    });

    // Funzione per chiudere il modale
    function closeModal() {
        document.getElementById('modal').style.display = 'none';
    }

    // Aggiungi un nodo al centro con etichetta
    document.getElementById('modal-ok-button').addEventListener('click', function () {
        var centerX = cy.width() / 2;
        var centerY = cy.height() / 2;
        var newNodeLabel = document.getElementById('modal-input').value;

        if (newNodeLabel.trim() !== '') {
            var newNode = cy.add({
                group: 'nodes',
                data: { id: 'node-' + cy.nodes().length, label: newNodeLabel },
                position: { x: centerX, y: centerY }
            });
            var actions = [{ type: 'addNode', nodeId: newNode.id() }];
            addAction(actions);
            logCurrentState();
        }

        closeModal();
    });

    // Chiudi il modale alla pressione del pulsante "Annulla"
    document.getElementById('modal-cancel-button').addEventListener('click', function () {
        closeModal();
    });

    // Cancella un nodo selezionato
    document.getElementById('deleteNodeButton').addEventListener('click', function () {
        var selectedNodes = cy.$('node:selected');
        if (selectedNodes.length > 0) {
            var actions = selectedNodes.map(node => ({
                type: 'deleteNode',
                data: node.data(),
                position: node.position()
            }));
            selectedNodes.remove();
            addAction(actions);
            logCurrentState();
        }
    });

    // Connetti i nodi tra loro
    var firstNode = null;

    cy.on('select', 'node', function (event) {
        if (!firstNode) {
            firstNode = event.target;
        }
    });

    document.getElementById('connectNodesButton').addEventListener('click', function () {
        if (firstNode) {
            var selectedNodes = cy.$('node:selected');
            var secondNode = selectedNodes.filter(node => node.id() !== firstNode.id())[0];

            if (secondNode) {
                var edgeId = 'edge-' + cy.edges().length;
                cy.add({
                    group: 'edges',
                    data: { id: edgeId, source: firstNode.id(), target: secondNode.id() }
                });

                var actions = [
                    { type: 'addEdge', edgeId: edgeId, sourceNodeId: firstNode.id(), targetNodeId: secondNode.id() }
                ];

                addAction(actions);
                logCurrentState();
            }

            // Resetta il primo nodo
            firstNode = null;
        }
    });

    // Elimina i collegamenti selezionati
    document.getElementById('deleteEdgeButton').addEventListener('click', function () {
        var selectedEdges = cy.$('edge:selected');
        if (selectedEdges.length > 0) {
            var actions = selectedEdges.map(edge => ({ type: 'deleteEdge', data: edge.data() }));
            selectedEdges.remove();
            addAction(actions);
            logCurrentState();
        }
    });

    // Cambia l'etichetta del nodo selezionato
    document.getElementById('modal-ok-button').addEventListener('click', function () {
        var selectedNodes = cy.$('node:selected');
        var newLabel = document.getElementById('modal-input').value;

        if (newLabel.trim() !== '') {
            var actions = selectedNodes.map(node => ({
                type: 'changeLabel',
                nodeId: node.id(),
                oldLabel: node.data('label'),
                newLabel: newLabel
            }));
            selectedNodes.data('label', newLabel);
            addAction(actions);
            logCurrentState();
        }

        closeModal();
    });

    // Abilita/disabilita il pulsante "Cambia Etichetta Nodi Selezionati" in base alla selezione dei nodi
    cy.on('select unselect', function () {
        var selectedNodes = cy.$('node:selected');
        var connectNodesButton = document.getElementById('connectNodesButton');
        var changeLabelButton = document.getElementById('openChangeLabelModalButton');
        var deleteNodesButton = document.getElementById('deleteNodeButton');
        var deleteEdgesButton = document.getElementById('deleteEdgeButton');
        connectNodesButton.disabled = selectedNodes.length !== 2;
        changeLabelButton.disabled = selectedNodes.length === 0;
        deleteNodesButton.disabled = selectedNodes.length === 0;

        var selectedEdges = cy.$('edge:selected');
        deleteEdgesButton.disabled = selectedEdges.length === 0;
    });

    // Annulla l'azione precedente
    document.getElementById('undoButton').addEventListener('click', function () {
        if (actionIndex >= 0) {
            var actionsToUndo = actionHistory[actionIndex];
            for (var i = actionsToUndo.length - 1; i >= 0; i--) {
                var action = actionsToUndo[i];
                if (action.type === 'changeLabel') {
                    var node = cy.$('#' + action.nodeId);
                    node.data('label', action.oldLabel);
                } else if (action.type === 'addNode') {
                    cy.$('#' + action.nodeId).remove();
                } else if (action.type === 'deleteNode') {
                    cy.$('#' + action.nodeId).remove();
                } else if (action.type === 'addEdge') {
                    cy.$('#' + action.edgeId).remove();
                } else if (action.type === 'deleteEdge') {
                    var edgeId = 'edge-' + cy.edges().length;
                    cy.add({
                        group: 'edges',
                        data: action.data
                    });
                    action.edgeId = edgeId;
                }
            }
            actionIndex--;
            logCurrentState();
        }
    });

    // Ripeti l'azione successiva
    document.getElementById('redoButton').addEventListener('click', function () {
        if (actionIndex < actionHistory.length - 1) {
            actionIndex++;
            var actionsToRedo = actionHistory[actionIndex];
            for (var i = 0; i < actionsToRedo.length; i++) {
                var action = actionsToRedo[i];
                if (action.type === 'changeLabel') {
                    var node = cy.$('#' + action.nodeId);
                    node.data('label', action.newLabel);
                } else if (action.type === 'addNode') {
                    cy.$('#' + action.nodeId).remove();
                } else if (action.type === 'deleteNode') {
                    var newNode = cy.add({
                        group: 'nodes',
                        data: action.data,
                        position: action.position
                    });
                    action.nodeId = newNode.id();
                } else if (action.type === 'addEdge') {
                    var edgeId = 'edge-' + cy.edges().length;
                    cy.add({
                        group: 'edges',
                        data: { id: edgeId, source: action.sourceNodeId, target: action.targetNodeId }
                    });
                    action.edgeId = edgeId;
                } else if (action.type === 'deleteEdge') {
                    var edgeId = 'edge-' + cy.edges().length;
                    cy.add({
                        group: 'edges',
                        data: action.data
                    });
                    action.edgeId = edgeId;
                }
            }
            logCurrentState();
        }
    });

    //// Funzioni per aggiungere un JSON a TheoryFlow

    // Funzione per aggiungere un nodo al grafo
    function addNodeToGraph(nodeData) {
        var newNode = cy.add({
            group: 'nodes',
            data: { id: nodeData.id, label: nodeData.label },
            position: {x: Math.random()*cy.width(), y: Math.random()*cy.height()},
        });

        var actions = [{ type: 'addNode', nodeId: newNode.id() }];
        addAction(actions);
        logCurrentState();
    }

    // Funzione per aggiungere un arco al grafo
    function addEdgeToGraph(edgeData) {
        var sourceNode = cy.nodes('[id="' + edgeData.source + '"]');
        var targetNode = cy.nodes('[id="' + edgeData.target + '"]');

        if (sourceNode.length > 0 && targetNode.length > 0) {
            var newEdge = cy.add({
                group: 'edges',
                data: { id: 'edge-' + cy.edges().length, source: sourceNode.id(), target: targetNode.id() }
            });

            var actions = [{ type: 'addEdge', edgeId: newEdge.id(), sourceNodeId: sourceNode.id(), targetNodeId: targetNode.id() }];
            addAction(actions);
            logCurrentState();
        }
    }

    // Funzione per aggiungere nodi e archi al grafo a partire da un oggetto JSON
    function addGraphFromObject(graphObject) {
        if (graphObject.nodes && Array.isArray(graphObject.nodes)) {
            // Aggiungi i nodi al grafo
            graphObject.nodes.forEach(function (node) {
                addNodeToGraph({ id: node.id, label: node.label });
            });
        }

        if (graphObject.edges && Array.isArray(graphObject.edges)) {
            // Aggiungi gli archi al grafo
            graphObject.edges.forEach(function (edge) {
                addEdgeToGraph({ source: edge.source, target: edge.target });
            });
        }
    }

    // Funzioni di esempio per simulare azioni e registrare lo stato
    function addAction(actions) {
        console.log('Action added:', actions);
    }

    function logCurrentState() {
        console.log('Current graph state:', cy.json());
    }

    // Esempio di utilizzo della funzione con l'oggetto JSON fornito
    var exampleGraphObject = {
        "nodes": [
            { "id": "A", "label": "Node A" },
            { "id": "B", "label": "Node B" },
            { "id": "C", "label": "Node C" },
            { "id": "D", "label": "Node D" }
        ],
        "edges": [
            { "source": "A", "target": "B" },
            { "source": "B", "target": "C" },
            { "source": "B", "target": "D" }
        ]
    };

    addGraphFromObject(exampleGraphObject);


});